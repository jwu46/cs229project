\documentclass{cs229report}
\usepackage{amsmath}
\DeclareMathOperator{\argmax}{arg\,max}

\begin{document}
\title{\textsc{Classification of Human Posture and Movement Using Accelerometer Data}}
\author{
  \footnotesize
  \parbox{3in}{
    \centering
    Huafei~Wang \\
    huafei@stanford.edu \\
    Dept. of Electrical Engineering \\
    Stanford University
  }
  \hspace*{-1in}
  \parbox{3in}{
    \centering
    Jennifer~Wu \\
    jwu46@stanford.edu \\
    Dept. of Aero/Astro \\
    Stanford University
  }
  \date{}
}
% make the title area
\maketitle

\section{Introduction}
Human activity classification has wide reaching applications,
such as in providing medical assistance to disabled or elderly persons.
This project implements several machine learning algorithms to 
classify human posture and movements. The different activities being classified are: 
\begin{center}
  Sitting,
  Sitting down,
  Standing,
  Standing up and
  Walking
\end{center}
The difference between ``Sitting'' and ``Sitting down'' is that the former
is the static posture, whereas the latter is the transitional movement
from standing to sitting.

\subsection*{Past work}
Bodor et al. developed a novel method for 
employing image-based rendering to extend the capability of 
the human movement classification \cite{bodor2009view}. 
Sminchisescu et al. have developed algorithms for recognizing human motion 
in monocular video sequences, based on discriminative 
conditional random fields (CRFs) and maximum entropy Markov models (MEMMs) \cite{sminchisescu2006conditional}. 

However, vision-based systems have issues in camera installation, lighting, 
picture quality, privacy and etc., 
which may render the system impractical in certain applications. 
As a branch of movement classification techniques, 
wearable sensor-based system could solve the above problems 
thanks to the development of nano-manufacturing technologies 
and ultralow-power embedded systems, which makes 
the wearable sensors cheap, small and compact. 
Ugulino et al. collected human movement data from 
4 ADXL335 accelerometers, utilized C4.5 tree, 
Iterative Dichotomiser 3 (ID3) and AdaBoost to classify 
the human movements and have obtained
high recall and precision \cite{Ugulino2012}.
This project utilizes the same set of data but a different
set of models to compare classification performance.

\section{Data}
The data is made publicly available on UCI's Machine Learning Repository.
It can be accessed at: 
\url{http://groupware.les.inf.puc-rio.br/har\#dataset}.

The dataset contains the following features
\begin{center}

  Age,
  Weight,
  Body Mass Index,
  Height \\
  x,y,z axis readings from 4 different accelerometers
\end{center}

\begin{table}[!h]
  \centering
  \caption{Frequency of each class}
  \begin{tabular}[]{|l|c|} \hline
    \bf {Class} & \bf {Frequency} \\ \hline
    Sitting & 50631 \\ \hline
    Sitting down & 11827 \\ \hline
    Standing & 47370 \\ \hline
    Standing up & 12415 \\ \hline
    Walking & 43390 \\ \hline
  \end{tabular}
\end{table}

% ============================= Features =====================================
\section{Features}
\subsection{Description}
The features used in our models are the $12$ accelerometer readings. Although the original data also contains age, weight, body mass index and height, they are neglected in this preliminary analysis and classification because they are less relevant in determining human movement compared with the $12$ accelerometer readings.

\subsection{Preprocessing}

In the initial implementation without any preprocessing of the data,
precision and recall were very low. 
To improve performance, different ways to scale the data was investigated.
More specifically, we tried {\em Z-score scaling} and {\em $0-1$ scaling}
and tested the resulting effects on performance.


\begin{table}[!h]
  \centering
  \caption{Effect of scaling data on performance}
  \begin{tabular}[]{|l|c|c|c|c|}
    \hline
    & \multicolumn{2}{|c|}{GDA} & \multicolumn{2}{|c|}{SVM} \\ \hline
    & Precision(\%) & Recall(\%) & Precision(\%) & Recall(\%) \\ \hline
    Unscaled & $78.94$ & $69.56$ & $85.83$ & $51.40$ \\ \hline
    Z-score scaling & $75.49$ & $70.42$ & $98.76$ & $98.76$ \\ \hline
    $0-1$ scaling & $99.90$ & $99.90$ &$99.90$ & $99.90$\\ \hline
  \end{tabular}
\end{table}


{\bf Randomizing the data}
Since the raw data is presented as a series of sampled outputs from 
the accelerometers,
it was important to randomize the order of the data sets,
especially when we were performing smaller tests where
only a small constant number of training example were chosen.
Due to the physical nature of the data,
consecutive training examples are largely dependent,
thus reducing the rank of the training matrix.
It was found that randomizing the order of the training sets
improved precision and recall.

% ================================ GDA =======================================
\subsection{Principal Component Analysis}
Since the feature data is of dimension 12, 
which makes it impossible to directly visualize the data, 
Principal Component Analysis (PCA) is used  
to reduce the dimension for the feature dataset from 12 to 3. 
Three principal eigenvalues and the associated eigenvectors are used. 
The data is represented in figure \ref{fig:pca}.
The horizontal data distribution corresponds to human lateral movements 
(moving forward, backward, left, right) 
while the vertical data distribution corresponds to human longitudinal movements 
(standing up, sitting down).

\begin{figure}[h]
  \centering
  \hspace*{-0.8in}
  \includegraphics[scale=0.4]{../pca.png}
  \caption{Data projected onto first 3 principal eigenvectors}
  \label{fig:pca}
\end{figure}

\section{Gaussian Discriminant Analysis}
\subsection{Binary classification}
GDA models the input features as a multivariate normal distribution,
with the class label as a Bernouilli variable.
For each class,
the class's data is used as positive training examples,
while data from each of the other classes are concatenated 
to be used as negative training examples.
After constructing the training set, 
model parameters are calculated with the following formulas:
\begin{eqnarray*}
  \phi &=& \frac{1}{m} \sum \limits_{i=1}^m 1 \lbrace y^{(i)} = 1 \rbrace\\
  \mu_0 &=& \frac{\sum \limits_{i=1}^m 1\lbrace y^{(i)} = 0 \rbrace x^{(i)}}
  {\sum \limits_{i=1}^m 1\lbrace y^{(i)} = 0 \rbrace} \, ;\quad
  \mu_1 = \frac{\sum \limits_{i=1}^m 1\lbrace y^{(i)} = 1 \rbrace x^{(i)}}
  {\sum \limits_{i=1}^m 1\lbrace y^{(i)} = 1 \rbrace} \\
  \Sigma&=& \frac{1}{m} \sum\limits_{i=1}^m 
  \left(x^{(i)} - \mu_{y^{(i)}} \right)
  \left(x^{(i)} - \mu_{y^{(i)}} \right)^T \\
\end{eqnarray*}

\subsection{Multi-class classification}
For a testing example, in order to make a prediction into 
$1$ of the $5$ classes, the posterior distribution is calculated for
each class, and the predicted label is chosen depending on the largest
posterior.

That is
\begin{equation*}
  h_\theta(x) = \argmax_y p(x|y)p(y); \quad \text{where } y \in \lbrace 1,2,3,4,5 \rbrace
\end{equation*}


\subsection*{Learning Curve}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{../LCgda.png}
  \caption{GDA learning curve}
  \label{fig:lcsvm}
\end{figure}
The learning curve shows significant improvement in performance
only up to the first $2000$ training examples. 
Thus SVM is efficient in terms of training set size.

\section{Support Vector Machine}
A support vector machine builds a model that seeks to maximize
the margin between the separating hyperplane and data points.
More specifically, the model parameters are found by
solving the following optimization problem\cite{lecnotes}:
\begin{eqnarray*}
  \min_{\gamma,w,b} & & \frac{1}{2}\parallel w \parallel ^2 +
  C \sum \limits_{i=1}^m \xi_i \\
  s.t. & & y^{(i)}(w^Tx^{(i)} + b ) \geq 1- \xi_i, i=1,..,m \\
  && \xi_i \geq 0, i=1,\dots,m
\end{eqnarray*}
To implement SVM, we used LIBSVM 
\cite{CC01a}. 
\subsection*{Learning Curve}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{../LCsvm.png}
  \caption{SVM learning curve}
  \label{fig:lcsvm}
\end{figure}


% ===================================k-means ==================================
\section{K-means}
Since a particular human movement usually requires a harmonious coordination among different parts of the body, accelerometer readings would exhibit clustering properties. Therefore, K-Means has been attempted to classify the human postures and movements. 
\subsection*{Learning Curve}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{../LCkmeans.png}
  \caption{K-means learning curve}
  \label{fig:lckmeans}
\end{figure}

The K-means learning curve is shown in figure \ref{fig:lckmeans}.
K-Means has a poor overall classification performance and 
the learning curve for the K-Means does not exhibit 
a typical decreasing pattern as most of the supervised learning algorithms do. 
One reason is that K-Means is not a supervised learning method and 
the clustering algorithm does not necessarily correspond 
to how human accelerations of a movement are coordinated and clustered. 
Therefore, K-Means could only roughly classify non-movements 
from the accelerometer readings while 
detailed movements classifications need to be done 
by using supervised learning methods like GDA and SVM. 
Therefore, it is concluded that unsupervised learning algorithms 
like K-Means may not be suitable in a supervised learning context.


\section{Results}
For the final results,
training was done on taking $90\%$ of the data
from each class and testing was done on the remaining
$10\%$ of the data. The following table shows the
precision and recall for each model.
\begin{table}[H]
%  \renewcommand{\arraystretch}{1}
  \caption{Testing results for all three models}
  \begin{tabular}[H]{|l|c|c|c|c|c|c|}
    \hline
    & \multicolumn{2}{|c|}{GDA}& \multicolumn{2}{|c|}{SVM} &
    \multicolumn{2}{|c|}{K-means} \\ \hline
    & Precision(\%) & Recall (\%) 
    & Precision(\%) & Recall (\%) & Precision(\%) & Recall (\%) \\ 
    \hline 
    Sitting & $99.94$  & $99.96$ & $99.98$ & $99.96$ & $82.0$ & $100.0$\\
    Sitting down & $99.83$ & $99.75$ & $99.83$ & $99.75$ & $0.0$ & $0.0$ \\
    Standing & $99.81$ & $100.00$ & $99.96$ & $100.00$ & $44.7$ & $100.0$ \\
    Standing up & $100.00$ & $99.12$ & $99.92$  & $99.84$ & $39.7$ & $37.3$ \\
    Walking & $99.95$ & $100.00$ & $99.98$ & $100.00$ & $2.9$ & $1.4$\\ \hline
  \end{tabular}
\end{table}

\section{Future Work}
The algorithms exhibited have shown high precision and recall. 
The next step would be to integrate the algorithm into a portable embedded system 
that has limited computation capability, 
memory space and/or battery life to classify the movements 
to make the system practical. 
With the development of the cloud technology, 
the wearable devices could also just collect the accelerometer data 
and send it wirelessly to a data processing server to 
carry out the processing and classification work. 
This would enable the devices to work longer and more reliably given the limited resources available.
\nocite{*}
\bibliography{reference}
\bibliographystyle{ieeetr}
\end{document}


