%% Huafei Wang PCA Implementation
clear all; close all; clc;

%% Prepare the Data
[data,classRange] = parseData;
%       0 - sitting
%       1 - sittingdown
%       2 - standing
%       3 - standingup
%       4 - walking

% Obtain Index for Different Categories
sitting_example_index = find(data.class == 0);
sittingdown_example_index = find(data.class == 1);
standing_example_index = find(data.class == 2);
standingup_example_index = find(data.class == 3);
walking_example_index = find(data.class == 4);

% Example Size of Different Categories
sitting_example_size = size(sitting_example_index, 1);
sittingdown_example_size = size(sittingdown_example_index, 1);
standing_example_size = size(standing_example_index, 1);
standingup_example_size = size(standingup_example_index, 1);
walking_example_size = size(walking_example_index, 1);

% Assert that the Training Size is Appropriate
example_size_matrix = [sitting_example_size; sittingdown_example_size; ...
    standing_example_size; standingup_example_size; walking_example_size];
training_size = 5000; % <<<============== Training Size (Each Category)
assert(training_size < min(example_size_matrix), 'Inappropriate Training Size.');

% Training Index
sitting_training_index = sitting_example_index([1 : training_size]);
sittingdown_training_index = sittingdown_example_index([1 : training_size]);
standing_training_index = standing_example_index([1 : training_size]);
standingup_training_index = standingup_example_index([1 : training_size]);
walking_training_index = walking_example_index([1 : training_size]);

% Testing Index
sitting_testing_index = sitting_example_index([training_size + 1 : sitting_example_size]);
sittingdown_testing_index = sittingdown_example_index([training_size + 1 : sittingdown_example_size]);
standing_testing_index = standing_example_index([training_size + 1 : standing_example_size]);
standingup_testing_index = standingup_example_index([training_size + 1 : standingup_example_size]);
walking_testing_index = walking_example_index([training_size + 1 : walking_example_size]);

%% Set up the training matrix
Training_Matrix = zeros(12, training_size * 5);
Training_Matrix = [data.x1(sitting_training_index)', data.x1(sittingdown_training_index)', ...
    data.x1(standing_training_index)', data.x1(standingup_training_index)', data.x1(walking_training_index)';
    data.y1(sitting_training_index)', data.y1(sittingdown_training_index)', ...
    data.y1(standing_training_index)', data.y1(standingup_training_index)', data.y1(walking_training_index)';
    data.z1(sitting_training_index)', data.z1(sittingdown_training_index)', ...
    data.z1(standing_training_index)', data.z1(standingup_training_index)', data.z1(walking_training_index)';
    data.x2(sitting_training_index)', data.x2(sittingdown_training_index)', ...
    data.x2(standing_training_index)', data.x2(standingup_training_index)', data.x2(walking_training_index)';
    data.y2(sitting_training_index)', data.y2(sittingdown_training_index)', ...
    data.y2(standing_training_index)', data.y2(standingup_training_index)', data.y2(walking_training_index)';
    data.z2(sitting_training_index)', data.z2(sittingdown_training_index)', ...
    data.z2(standing_training_index)', data.z2(standingup_training_index)', data.z2(walking_training_index)';
    data.x3(sitting_training_index)', data.x3(sittingdown_training_index)', ...
    data.x3(standing_training_index)', data.x3(standingup_training_index)', data.x3(walking_training_index)';
    data.y3(sitting_training_index)', data.y3(sittingdown_training_index)', ...
    data.y3(standing_training_index)', data.y3(standingup_training_index)', data.y3(walking_training_index)';
    data.z3(sitting_training_index)', data.z3(sittingdown_training_index)', ...
    data.z3(standing_training_index)', data.z3(standingup_training_index)', data.z3(walking_training_index)';
    data.x4(sitting_training_index)', data.x4(sittingdown_training_index)', ...
    data.x4(standing_training_index)', data.x4(standingup_training_index)', data.x4(walking_training_index)';
    data.y4(sitting_training_index)', data.y4(sittingdown_training_index)', ...
    data.y4(standing_training_index)', data.y4(standingup_training_index)', data.y4(walking_training_index)';
    data.z4(sitting_training_index)', data.z4(sittingdown_training_index)', ...
    data.z4(standing_training_index)', data.z4(standingup_training_index)', data.z4(walking_training_index)'];

%% Pre-process the Matrix

% Mean
u = sum(Training_Matrix, 2) / (training_size * 5);
u_Matrix = zeros(12, training_size * 5);
for i = 1 : (training_size * 5)
    u_Matrix(:, i) = u;
end
T = Training_Matrix - u_Matrix;
% Variance
T_variance = T .^ 2;
variance = sum(T_variance, 2) / (training_size * 5);
v_Matrix = zeros(12, training_size * 5);
for i = 1 : (training_size * 5)
    v_Matrix(:, i) = variance;
end
T = T ./ v_Matrix;
%% Calculate the Emprical Covariance Matrix 25000 * 25000
C = zeros(12, 12);
for i = 1 : training_size * 5
    C = C + T(:, i) * T(:, i)';
end

%% Dimension Reduction
[V, D] = eigs(C, 3); % D - Eigenvalues; V - Eigenvectors
New_Training_Matrix = V' * T;

marker = ['*', '+', 'o', '.', '^'];
color = ['b', 'r', 'k', 'g', 'm'];
%figure();
%
for i = 1 : 5
    plot3(New_Training_Matrix(1, ((i - 1) * 5000 + 1 : i * 5000)), New_Training_Matrix(2, ((i - 1) * 5000 + 1 : i * 5000)), ...
        New_Training_Matrix(3, ((i - 1) * 5000 + 1 : i * 5000)), 'LineStyle', 'none', 'Marker', marker(i), 'MarkerEdgeColor', color(i));
    hold on;
end
legend('Class1', 'Class2', 'Class3', 'Class4', 'Class5');



