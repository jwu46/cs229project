function plotLearningCurve(data)
% clear; clc;
nClasses = 5;
% mtrain_vec = [500 1000];
mtrain_vec = 25000;
mTest = 5000; % test set size

% initialize some empty vectors
testErrorSVM = zeros(length(mtrain_vec),1);
trainErrorSVM = zeros(length(mtrain_vec),1);
testErrorGDA = zeros(length(mtrain_vec),1);
trainErrorGDA = zeros(length(mtrain_vec),1);

i = 1;
for m = mtrain_vec
    [testErrorSVM(i), trainErrorSVM(i), ~, ~] = runSVM(data, floor(m/nClasses), floor(mTest/nClasses));
    [testErrorGDA(i), trainErrorGDA(i), ~, ~] = runGDA(data, floor(m/nClasses), floor(mTest/nClasses));
    i = i + 1;
end
figure(1)
semilogx(mtrain_vec, trainErrorSVM, 'b+')
hold on; grid on
semilogx(mtrain_vec, testErrorSVM, 'r+')
legend('Training Error', 'Test Error')
quickLabel('m', 'error (\%)', 'SVM')

figure(2)
semilogx(mtrain_vec, trainErrorGDA, 'b+')
hold on; grid on
semilogx(mtrain_vec, testErrorGDA, 'r+')
legend('Training Error', 'Test Error')
quickLabel('m', 'error (\%)', 'GDA')

save('learningcurve.mat', 'mtrain_vec')
save('learningcurve.mat', 'testErrorGDA', '-append')
save('learningcurve.mat', 'trainErrorGDA', '-append')
save('learningcurve.mat', 'testErrorSVM', '-append')
save('learningcurve.mat', 'trainErrorSVM', '-append')
end