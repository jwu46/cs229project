function pca2
close all
[data,~] = parseData;
iStart = 1;
iEnd = 5000;
[X,Y] = sliceData(data,iStart,iEnd);
X = scaleData(X,'standard');
m = size(X,1);
C = 1/m * X'*X;
[V,~] = eigs(C,3);
X2 = X*V(:,1:2);
X3 = X*V;

% lets now visualize the data
marker = ['*', '+', 'o', '.', '^'];
color = ['b', 'r', 'k', 'g', 'm'];

figure
for j = 1:5    
    plot(X2(Y==j,1), X2(Y==j,2),...
        'Linestyle', 'none',...
        'Marker', marker(j),...
        'MarkerEdgeColor', color(j));
    hold on
    grid on
end
legend('Sitting', 'Sitting down' ,'Standing', 'Standing up', 'Walking')

figure
for j = 1:5    
    plot3(X3(Y==j,1), X3(Y==j,2), X3(Y==j,3),...
        'Linestyle', 'none',...
        'Marker', marker(j),...
        'MarkerEdgeColor', color(j));
    hold on
    grid on
end
legend('Sitting', 'Sitting down' ,'Standing', 'Standing up', 'Walking')

end