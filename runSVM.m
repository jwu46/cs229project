function [testError, trainError, prec, acc] = runSVM(data, mTrain, mTest)
nClasses = 5;
% set up system path so it finds LIBSVM bin
path1 = getenv('PATH');
path1 = [path1 ':/usr/local/bin'];
setenv('PATH', path1)

if nargin < 3
    mTest = 1000;
end
% check for m
mMAX = 11827;
if mTrain+mTest > mMAX
    error('NotEnoughTrainingEx',...
        'We don''t have that many training examples!');
end
% fprintf('Training SVM...\n');
featureNames = {...
    'x1'; 'y1'; 'z1';
    'x2'; 'y2'; 'z2';
    'x3'; 'y3'; 'z3';
    'x4'; 'y4'; 'z4'};
% we need to scale all data
% doing that in svm-scale for now

% data = cell, each cell is a struct with data

%
% -- TRAINING --
%
% Write data in a friendly format
iStart = 1;
iEnd = iStart + mTrain - 1;
[X,Y] = sliceData(data,iStart,iEnd);

fname = 'svmTrainData';
fid = fopen(fname, 'w');
for i = 1:size(X,1)
    x = X(i,:);
    fprintf(fid, '+%d', Y(i));
    for j = 1:length(x);
        fprintf(fid, '\t%d:%.6e', j,x(j));
    end
    fprintf(fid, '\n');
end
fclose(fid);


iStart = 6000 - mTest + 1;
iEnd = 6000;
[X,Y] = sliceData(data,iStart,iEnd);
% -------------------------------------------------------------------------
% commented out writing test file, since it's always the same
% do something smart about this later
% -------------------------------------------------------------------------
fprintf('\n\n Writing testing file...');
fname = 'svmTestData';
fid = fopen(fname, 'w');
for i = 1:size(X,1)
    x = X(i,:);
    fprintf(fid, '+%d', Y(i));
    for j = 1:length(x);
        fprintf(fid, '\t%d:%.6e', j,x(j));
    end
    fprintf(fid, '\n');
end
fclose(fid);
% -------------------------------------------------------------------------

!./svm.sh svmTrainData svmTestData

% TEST error
fname = 'svmout_test';
fid = fopen(fname, 'r');
s = fgets(fid);
[etest, ~] = sscanf(s, '%*s %*c %f %% %*c %d%*c%d');
fclose(fid);

% get confusion matrix, prec and acc on TEST set
cm = zeros(nClasses);

% read out file
%********* FINISH THIS

fname = 'svmTestData.out';
fid = fopen(fname, 'r');
rawdata = textscan(fid, '%d');
rawdata = rawdata{1};
for i = 1:size(X,1)
    cm(Y(i), rawdata(i)) = cm(Y(i), rawdata(i)) + 1;
end
fclose(fid);

disp(cm)
for j = 1:nClasses
    prec(j) = cm(j,j) / sum(cm(j,:));
    acc(j) = cm(j,j) / sum(cm(:,j));
end

% TRAINING error
fname = 'svmout_train';
fid = fopen(fname, 'r');
s = fgets(fid);
[etrain, ~] = sscanf(s, '%*s %*c %f %% %*c %d%*c%d');
fclose(fid);

testError = 1 - etest(1)/100;
trainError = 1 - etrain(1)/100;

% !./svm.sh svmTrainData svmTestData

end
