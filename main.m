% CS229 | Project | Jennifer Wu |10/19/14

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- Initialize ----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cd '/Users/jwu/Dropbox/fall14/cs229/project'
if ~exist('trainingdata', 'var')
    [trainingdata,classRange] = parseData;
end
data = scaleData(trainingdata);
% data = trainingdata;

% data has the following fields:
%   name
%   gender
%   age
%   height
%   weight
%   bmi
%   x1,y1,z1
%   x2,y2,z2
%   x3,y3,z3
%   x4,y4,z4
%   class:
%       0 - sitting
%       1 - sittingdown
%       2 - standing
%       3 - standingup
%       4 - walking

% index range for classes
% classRange =
%            1       50631 m = 50631
%        50632       62458 m = 11827
%        62459      109828 m = 47370
%       109829      122243 m = 12415
%       122244      165633 m = 43390


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- GDA model ----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[etest, etrain, prec, acc] = runGDA(data,5000,1000);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- Train softmax ----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [model, resid, confusionMatrix] = trainSoftmax(data,100);
% trainSoftmaxStochastic(data);
% confusionMatrix = softmaxTrial(data);
% save('cm.mat', 'confusionMatrix');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Issues to address:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2. Rewrite to modularize function for just formatting the data
%   e.g. X = extractTrainingData

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- SVM model ----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,~, prec,acc]=runSVM(data, 5000, 1000);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- Learning curve ----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plotLearningCurve(data)

%% plots for poster
clear; close all
load('learningcurve.mat')
figure(1)
plot(mtrain_vec, trainErrorSVM*100, 'b+', 'markersize', 16)
hold on; grid on
plot(mtrain_vec, testErrorSVM*100, 'r+', 'markersize', 16)
legend('Training Error', 'Test Error')
quickLabel('m', 'error (\%)', 'SVM')

figure(2)
plot(mtrain_vec, trainErrorGDA*100, 'b+', 'markersize', 16)
hold on; grid on
plot(mtrain_vec, testErrorGDA*100, 'r+', 'markersize', 16)
legend('Training Error', 'Test Error')
quickLabel('m', 'error (\%)', 'GDA')

export(30,2)
